﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Maps
{
    public partial class MapSelectPage : ContentPage
    {
        Coordinates selectedCoordinates = new Coordinates(0, 0);
        Dictionary<string, Coordinates> locationLookup;

        public MapSelectPage()
        {
            InitializeComponent();
            PopulatePicker();
        }

        class Coordinates {
            public Coordinates(double lat, double lng) {
                this.lat = lat;
                this.lng = lng;
            }

            public double lat; //Latitude
            public double lng; //Longitude
        }

        private void PopulatePicker()
        {
            this.locationLookup = new Dictionary<string, Coordinates>()
            {
                { "Reckless Brewing", new Coordinates(32.8879902, -117.1595957)},
                { "Pie'n Burger", new Coordinates(34.1360106, -118.1336722)},
                { "Space Needle", new Coordinates(47.6205099, -122.3514661)},
                { "Downtown Ventura", new Coordinates(34.2789932, -119.2954436)},
                { "Hoh National Forest", new Coordinates(47.8608741, -123.9369914)}
            };


            foreach (var item in locationLookup)
            {
                LocationPicker.Items.Add(item.Key);
            }

            LocationPicker.SelectedIndex = 0;
        }

        public void Handle_NavigateToMap(object sender, EventArgs e) {
            Navigation.PushAsync(new MapViewPage());
        }

        public void Handle_SelectedIndexChanged(object sender, EventArgs e) {
            string location = LocationPicker.Items[LocationPicker.SelectedIndex];
            this.selectedCoordinates = this.locationLookup[location];
        }
    }
}
