﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Maps
{
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapViewPage : ContentPage
    {
        public MapViewPage()
        {
            InitializeComponent();

            var initialLocation = MapSpan.FromCenterAndRadius(new Position(37.8879902, -122.1595957), Distance.FromMiles(1000));
            Map.MoveToRegion(initialLocation);
            PlaceMarker(32.8879902, -117.1595957, "Reckless Brewing", "");
            PlaceMarker(34.1360106, -118.1336722, "Pie'n Burger", "");
            PlaceMarker(47.6205099, -122.3514661, "Space Needle", "");
            PlaceMarker(34.2789932, -119.2954436, "Downtown Ventura", "");
            PlaceMarker(47.8608741, -123.9369914, "Hoh National Forest", "");
        }

        private void PlaceMarker(double lat, double lng, string label, string address)
        {
            var position = new Position(lat, lng); // Latitude, Longitude
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = label,
                Address = address
            };


            Map.Pins.Add(pin);
        }

        public void Handle_HybridView(object sender, EventArgs e)
        {
            Map.MapType = MapType.Hybrid;
        }

        public void Handle_SatelliteView(object sender, EventArgs e)
        {
            Map.MapType = MapType.Satellite;
        }

        public void Handle_StreetView(object sender, EventArgs e)
        {
            Map.MapType = MapType.Street;
        }
    }
}
